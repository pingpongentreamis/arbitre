import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';


import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//Composants
import { BanniereComponent } from './Components/banniere/banniere.component';
import { LoginComponent } from './Pages/login/login.component';

//Service
import { ArbitreCRUDService } from './Services/API-CALL/arbitre-crud.service';
import { AuthService } from './Services/auth.service';
import { HttpClientModule } from '@angular/common/http';
import { AuthGuard } from './Services/auth.guard';

//Material
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import { ChangePassComponent } from './Pages/change-pass/change-pass.component';
import { MatchComponent } from './Pages/match/match.component';
import {MatCardModule} from '@angular/material/card';
import { PartieCRUDService } from './Services/API-CALL/partie-crud.service';
import { PointCRUDService } from './Services/API-CALL/point-crud.service';
import { MatchCaseComponent } from './Components/match-case/match-case.component';

@NgModule({
  declarations: [
    AppComponent,
    BanniereComponent,
    LoginComponent,
    ChangePassComponent,
    MatchComponent,
    MatchCaseComponent,
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
    HttpClientModule
  ],
  providers: [ ArbitreCRUDService,
              PartieCRUDService,
              PointCRUDService,
              AuthService,
              AuthGuard ],
  bootstrap: [AppComponent]
})
export class AppModule { }
