import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatchCaseComponent } from './match-case.component';

describe('MatchCaseComponent', () => {
  let component: MatchCaseComponent;
  let fixture: ComponentFixture<MatchCaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatchCaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatchCaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
