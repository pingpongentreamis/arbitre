import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PartieCRUDService } from 'src/app/Services/API-CALL/partie-crud.service';

@Component({
  selector: 'app-match-case',
  templateUrl: './match-case.component.html',
  styleUrls: ['./match-case.component.scss']
})
export class MatchCaseComponent implements OnInit {
  @Input() match: any;
  @Output() etat = new EventEmitter<boolean>();

  textInfo = '';

  constructor(private partie:PartieCRUDService) { }

  ngOnInit() {
    this.textInfo = this.match.etat ? 'Démarré' : 'Arrété'
  }

  btnBehaviour() {
    this.partie.updateEtat(this.match.id, Number(this.match.arbitre.id), this.match.etat === 1 ? 0 : 1).subscribe((data) => {
      this.etat.emit(true);
    });
}

}
