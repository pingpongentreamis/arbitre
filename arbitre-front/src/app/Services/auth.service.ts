import { Injectable } from '@angular/core';
import { ArbitreCRUDService } from './API-CALL/arbitre-crud.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private loggedInStatus = false;

  constructor(private arbitre: ArbitreCRUDService, ) { }

  setLoggedIn(value: boolean) {
    this.loggedInStatus = value
  }

  get isLoggedIn() {
    return this.loggedInStatus
  }

  getUserForlogin(nom: string, pass: string) {
    return this.arbitre.getArbitreForlogin(nom, pass);
  }
}
