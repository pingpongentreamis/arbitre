import { TestBed } from '@angular/core/testing';

import { ArbitreCRUDService } from './arbitre-crud.service';

describe('ArbitreCRUDService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ArbitreCRUDService = TestBed.get(ArbitreCRUDService);
    expect(service).toBeTruthy();
  });
});
