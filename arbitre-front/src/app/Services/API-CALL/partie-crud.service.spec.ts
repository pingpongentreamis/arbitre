import { TestBed } from '@angular/core/testing';

import { PartieCrudService } from './partie-crud.service';

describe('PartieCrudService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PartieCrudService = TestBed.get(PartieCrudService);
    expect(service).toBeTruthy();
  });
});
