import { TestBed } from '@angular/core/testing';

import { PointCrudService } from './point-crud.service';

describe('PointCrudService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PointCrudService = TestBed.get(PointCrudService);
    expect(service).toBeTruthy();
  });
});
