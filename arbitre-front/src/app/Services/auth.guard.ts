import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';
import { catchError, map, tap } from 'rxjs/operators';
import { handleError } from '../Function-utils/LogError';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private auth: AuthService,
    private router: Router,
    private http:HttpClient){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      
      if (this.auth.isLoggedIn){
        return this.auth.isLoggedIn;
      } else {
        this.router.navigate(['login']);
      }
        this.getToken().pipe(map(res => {
          if (res.valid) {
            this.auth.setLoggedIn(true);
            this.router.navigate(['match']);
          } else {
            this.router.navigate(['login']);
          }
        }));
        
      
  }

  /**
 * getPartieById
 * Recupère une partie avec un id
 * @param id - id of the Parties
 */
getToken(): Observable<any> {
  return  this.http.get<any>(environment.url + '/verifyArbitreToken/').pipe(
    tap(_ => console.log(`fetched token `)),
    catchError(handleError<any>(`getToken`))
  );
}
}
