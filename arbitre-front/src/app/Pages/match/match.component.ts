import { Component, OnInit } from '@angular/core';
import { PointCRUDService } from 'src/app/Services/API-CALL/point-crud.service';
import { PartieCRUDService } from 'src/app/Services/API-CALL/partie-crud.service';
import { PointModele } from 'src/app/Modele/PointModele';

@Component({
  selector: 'app-match',
  templateUrl: './match.component.html',
  styleUrls: ['./match.component.scss']
})
export class MatchComponent implements OnInit {
  matchsArray = [];
  matchs = [];

  j1 = '';
  j2 = '';
  idj1 = 0;
  idj2 = 0;
  scoreJ2 = 0;
  scoreJ1 = 0;
  sport = '';
  today: number = Date.now();
  partieArbitre = '';
  btn = false;
  id: any;
  idElem: any;

  constructor(private point: PointCRUDService, private partie: PartieCRUDService) { }

  ngOnInit() {
    this.setDataPoints();

  }

  setDataPoints() {
    this.btn = false;
    this.j1 = '';
    this.j2 = '';
    this.idj1 = 0;
    this.idj2 = 0;
    this.scoreJ2 = 0;
    this.scoreJ1 = 0;
    this.partieArbitre = '';
    this.matchs = [];
    this.matchsArray = [];
    this.partie.getPartie().subscribe( (data) => {
      const context = this;
      data.forEach(function (element) {
        if (element.arbitre.id.toString() === sessionStorage.getItem('id') ) {
          context.matchsArray.push(element);
          if (element.etat === 1) {
            context.btn = true;
            context.id = context.matchsArray.length-1;
            context.idElem = element.id.toString();
            context.idj1 = element.joueur1.ID;
            context.idj2 = element.joueur2.ID;
            context.j1 = element.joueur1.Nom;
            context.j2 = element.joueur2.Nom;
            context.scoreJ1 = element.score_j1.totalPoints;
            context.scoreJ2 = element.score_j2.totalPoints;
            context.partieArbitre = element.id;
            context.sport = element.sport.Label;
          }
        }
      });
      this.matchs = this.matchsArray;
    });
  }

  addPoint(joueur, nbj) {
    let pointJoueur: PointModele = new PointModele();
    pointJoueur.JoueurId = joueur;
    pointJoueur.PartieId = this.partieArbitre;
    this.point.insertPoint(pointJoueur).subscribe(() => {
      this.point.getPointForPlayerByPartie(joueur, Number(this.partieArbitre)).subscribe((data) => {
        if (nbj) {
          this.scoreJ1 = data.totalPoints;
        } else {
          this.scoreJ2 = data.totalPoints;
        }
      });
    });
    this.updateData();
  }

  deletePoint(joueur, nbj) {
    this.point.deletePoint(Number(this.partieArbitre), joueur).subscribe(() => {
      this.point.getPointForPlayerByPartie(joueur, Number(this.partieArbitre)).subscribe((data) => {
        if (nbj) {
          this.scoreJ1 = data.totalPoints;
        } else {
          this.scoreJ2 = data.totalPoints;
        }
      });
    });
    this.updateData();
  }

  updateData(){
    this.partie.getPartie().subscribe( (data) => {
      const context = this;
      data.forEach(function (element) {      
        if (element.id.toString() === context.idElem) {
          context.matchsArray[context.id]=element;
        }
      });
      this.matchs = this.matchsArray;
    });
  }

  onEtat(agreed: boolean) {
    this.setDataPoints();
  }
}
