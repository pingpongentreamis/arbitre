import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/Services/auth.service';
import { ArbitreCRUDService } from 'src/app/Services/API-CALL/arbitre-crud.service';
import { AuthGuard } from 'src/app/Services/auth.guard';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  registerForm: FormGroup;
  submitted: boolean;
  hide = true;
  loginInvalid = false;
  passInvalid = false;
  
  constructor(private formBuilder: FormBuilder,
              private auth: AuthService,
              private router: Router,
              private arbitre: ArbitreCRUDService) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      login: ['', [Validators.required, Validators.required]],
      password: ['', [Validators.required, Validators.minLength(5)]]
  });
   
  // this.arbitre.getArbitres().subscribe((data) => { console.log(data);});
  }

      // convenience getter for easy access to form fields
      get f() { return this.registerForm.controls; }

      onSubmit() {
        this.loginInvalid = false;
        this.passInvalid = false;
        this.submitted = true;

        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }
        this.auth.getUserForlogin(this.registerForm.get('login').value, this.registerForm.get('password').value).subscribe(
          (data) => {
            console.log(data);
              if (!data.login) { this.loginInvalid = true; return; } else if (!data.password) { this.passInvalid = true; return;}
              this.auth.setLoggedIn(data.password);
              sessionStorage.setItem('id', data.idArbitre.toString());
              this.arbitre.getArbitreById(data.idArbitre).subscribe((data) => {
                console.log(data);
                sessionStorage.setItem('nom', data.nom_arbitre);
              });
              this.router.navigate(['match']);
            });
    }
}
