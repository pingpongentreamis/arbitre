import { JoueurModele } from './JoueurModele';

export class PartieModele {
    organisation: string;
    arbitre: string;
    joueur1: JoueurModele;
    joueur2: JoueurModele;
    sport: string;
    etat: string;
    datetime: string;
}