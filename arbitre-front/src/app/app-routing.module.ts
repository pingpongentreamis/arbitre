import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './Pages/login/login.component';
import { ChangePassComponent } from './Pages/change-pass/change-pass.component';
import { MatchComponent } from './Pages/match/match.component';
import { AuthGuard } from './Services/auth.guard';

const routes: Routes = [{
   path: 'login', component: LoginComponent },
{ path: 'chpwd',        component: ChangePassComponent },
{ path: 'match', component: MatchComponent, canActivate: [AuthGuard] },
{ path: '',   redirectTo: 'login', pathMatch: 'full' },
{ path: '**', component: LoginComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
